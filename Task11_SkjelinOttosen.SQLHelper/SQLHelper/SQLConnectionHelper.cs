﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Task11_SkjelinOttosen.Model;

namespace Task11_SkjelinOttosen.SQLHelper
{
    // Helper class that opens connections to the database and performs CRUD operations
    public class SQLConnectionHelper
    {
        // Connection string for the database
        public static string ConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder
            {
                DataSource = @"PC7383\SQLEXPRESS",
                InitialCatalog = "RPG_Character",
                IntegratedSecurity = true
            };

            // Returns the connection string for the database connection
            return builder.ConnectionString.ToString();
        }

        // Insert new character
        public static bool Insert(RPG_Character character)
        {        
            try
            {
                // Connection to the database 
                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    // Opens connection
                    connection.Open();
                    Console.WriteLine("Connection open.... \n");

                    // Split namespace class object to object name
                    string characterType = character.GetType().ToString().Split('.')[2];
                    Console.WriteLine(characterType);

                    // Inserts new character
                    string sql = $"INSERT INTO {characterType} (Name, HP, Energy, AttackPower, Armor) VALUES (@CharacterName, @HP, @Energy, @AttackPower, @Armor);";
             
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {             
                        // Adding template for the SQL statements
                        command.Parameters.AddWithValue("@CharacterName", character.Name);
                        command.Parameters.AddWithValue("@HP", character.HP);
                        command.Parameters.AddWithValue("@Energy", character.Energy.ToString());
                        command.Parameters.AddWithValue("@AttackPower", character.AttackPower);
                        command.Parameters.AddWithValue("@Armor", character.Armor);
                        command.ExecuteNonQuery();                          
                    }

                    Console.WriteLine("Added to database");
                    return true;
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);     
            }
            return false;
        }

        // Gets all characters
        public static RPG_CharactersCollection GetAllCharacters(CharacterTypeEnum characterType)
        {               
            try
            {
                // Initialize  RPG_CharactersCollection
                RPG_CharactersCollection rpg_CharactersCollection = new RPG_CharactersCollection();

                // Connection to the database 
                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    // Opens connection
                    connection.Open();
                    Console.WriteLine("Connection open.... \n");

                    //SELECT all Warriors Object
                    string sql = $"SELECT * FROM {characterType}";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int id = reader.GetInt32(0);
                                string name = reader.GetString(1);
                                int hp = reader.GetInt32(2);
                                EnergyEnum energy = InputCheckHelper.CheckEnergyInput(reader.GetString(3));
                                int attackPower = reader.GetInt32(4);
                                int armor = reader.GetInt32(5);                  

                                // Creates empty object from user input
                                RPG_Character character = InputCheckHelper.CreateEmptyRPG_CharacterFromInput(characterType.ToString());
                               
                                // Updates object with values from the database
                                character.Id = id;
                                character.HP = hp;
                                character.Name = name;
                                character.Energy = energy;
                                character.AttackPower = attackPower;
                                character.Armor = armor;

                                // Adds object to teh collection
                                rpg_CharactersCollection.Add(character);
                            }
                        }
                    }
                }
                // Return collection
                return rpg_CharactersCollection;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        // Gets character at the given id
        public static RPG_Character GetCharacter(CharacterTypeEnum characterType, int characterId)
        {
            try
            {
                // Connection to the database 
                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    // Opens connection
                    connection.Open();
                    Console.WriteLine("Connection open.... \n");

                    //SELECT all Warriors Object
                    string sql = $"SELECT * FROM {characterType} WHERE Id = @CharacterId";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Adding template for the SQL statements
                        command.Parameters.AddWithValue("@CharacterId", characterId);
                        command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int id = reader.GetInt32(0);
                                string name = reader.GetString(1);
                                int hp = reader.GetInt32(2);
                                EnergyEnum  energy= InputCheckHelper.CheckEnergyInput(reader.GetString(3));
                                int attackPower = reader.GetInt32(4);
                                int armor = reader.GetInt32(5);

                                // Creates empty object from user input
                                RPG_Character character = InputCheckHelper.CreateEmptyRPG_CharacterFromInput(characterType.ToString());
                           
                                // Updates object with values from the database
                                character.Id = id;
                                character.Name = name;
                                character.HP = hp;
                                character.Energy = energy;
                                character.AttackPower = attackPower;
                                character.Armor = armor;

                                return character;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        // Updates the character
        public static bool UpdateCharacter(CharacterTypeEnum characterType, int characterId, string newName, int newHP, EnergyEnum newEnergy, int newAttackPower, int newArmor)
        {         
            try
            {
                // Connection to the database 
                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    // Opens connection
                    connection.Open();
                    Console.WriteLine("Connection open.... \n");

                    //SELECT all Warriors Object
                    string sql = $"UPDATE {characterType} SET Name = @Name, HP = @HP, Energy =  @Energy, AttackPower = @AttackPower, Armor = @Armor  WHERE Id= @CharacterId";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Adding template for the SQL statements
                        command.Parameters.AddWithValue("@Name", newName);
                        command.Parameters.AddWithValue("@HP", newHP);
                        command.Parameters.AddWithValue("@Energy", newEnergy.ToString());
                        command.Parameters.AddWithValue("@AttackPower", newAttackPower);
                        command.Parameters.AddWithValue("@Armor", newArmor);
                        command.Parameters.AddWithValue("@CharacterId", characterId);
                        command.ExecuteNonQuery();

                        return true;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        // Deletes the character with the given id
        public static bool DeleteCharacter(RPG_Character character)
        {
            try
            {
                // Connection to the database 
                using (SqlConnection connection = new SqlConnection(ConnectionString()))
                {
                    // Opens connection
                    connection.Open();
                    Console.WriteLine("Connection open.... \n");

                    // Split namespace class object to object name
                    string characterType = character.GetType().ToString().Split('.')[2];
                    Console.WriteLine(character.Id);

                    //SELECT all Warriors Object
                    string sql = $"DELETE FROM {characterType} WHERE Id= @CharacterId";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Adding template for the SQL statements          
                        command.Parameters.AddWithValue("@CharacterId", character.Id);
                        command.ExecuteNonQuery();
                        Console.WriteLine("Deleted");
                    }
                    return true;
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
    }
}
