﻿using System;
using System.Windows.Forms;

namespace Task11_SkjelinOttosen
{
    // Meny window for the application
    public partial class MenuForm : Form
    {
        public MenuForm()
        {
            InitializeComponent();
        }

        // Shows the create character window when clicked
        private void btnNew_Click(object sender, EventArgs e)
        {
            Program.createForm.Show();
        }

        // Shows the create character window when clicked
        private void btn_View_Click(object sender, EventArgs e)
        {
          
            Program.viewForm.Show();
        }

        private void MenuForm_Load(object sender, EventArgs e)
        {

        }
    }
}
