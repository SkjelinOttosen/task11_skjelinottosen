﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using CsvHelper;
using Task11_SkjelinOttosen.Model;
using Task11_SkjelinOttosen.SQLHelper;

namespace Task11_SkjelinOttosen
{
    // User interface for creating a character
    // Writes to a database or a csv as a backup
    public partial class CreateForm : Form
    {
        public CreateForm()
        {     
            InitializeComponent();
            Program.menuForm.Hide();
            
            // Adds values to combobox 
            cbCharacterType.Items.Add("Warrior");
            cbCharacterType.Items.Add("Thief");
            cbCharacterType.Items.Add("Wizard");
        }

        // Creates RPG Character 
        private RPG_Character CreateCharacter(string characterType)
        {
            // Input from user
            string nameSelected = ipName.Text.ToString();
          
            // Initialize empty objects
            RPG_Character character = null;
           
            character = InputCheckHelper.CreateEmptyRPG_CharacterFromInput(characterType);
            character.Name = nameSelected;

            return character;
        }
      
        // Creates character and writes record to the database, or a local CSV file if the SQL conetion fails
        private void btnCreate_Click(object sender, EventArgs e)
        {
            string type = cbCharacterType.SelectedItem.ToString();
            RPG_Character character = CreateCharacter(type);
            
            if (SQLConnectionHelper.Insert(character))
            {
                lbStatus.Text = $"Added {character.ToString()}";
            }
            else
            {          
                WriteToCSV(type, character, character.Weapon);
            }
        }

        // Write records to  local CSV file 
        public void WriteToCSV(string filename, RPG_Character character, Weapon weapon) 
        {
            try
            {
                // Creates one csv file for each object type. Object type is passed as argument filename
                string csvFile = @""+filename+".csv";
              
                if (!File.Exists(csvFile))
                {
                    //Creates the file
                    using (Stream stream = File.Create(csvFile))
                    {
                        // Initialize a StreamWriter 
                        using (StreamWriter fileWriter = new StreamWriter(stream))
                        {
                            // Initialize CsvWriter 
                            using (CsvWriter csvWriter = new CsvWriter(fileWriter, CultureInfo.InvariantCulture))
                            {
                                //Write header
                                csvWriter.WriteHeader<RPG_Character>();
                                csvWriter.NextRecord();
                            }
                        }
                    };
                    string path = Path.GetFullPath(csvFile);
                    MessageBox.Show($"Created {csvFile} in folder {path}");
                }

                // Initialize a Stream
                using (Stream stream = File.Open(csvFile, FileMode.Append))
                {
                    // Initialize a StreamWriter 
                    using (StreamWriter fileWriter = new StreamWriter(stream))
                    {
                        // Initialize CsvWriter 
                        using (CsvWriter csvWriter = new CsvWriter(fileWriter, CultureInfo.InvariantCulture))
                        {
                            //Writes records
                            csvWriter.WriteRecord<RPG_Character>(character);
                            MessageBox.Show($"{cbCharacterType.SelectedItem.ToString()} { ipName.Text.ToString()} added successfully");
                            csvWriter.NextRecord();
                        }
                    }
                } 
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // Return to the menu
        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Close();
            Program.menuForm.Show();
        }

        private void ipName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
