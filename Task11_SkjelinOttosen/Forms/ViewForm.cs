﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task11_SkjelinOttosen.Model;
using Task11_SkjelinOttosen.SQLHelper;

namespace Task11_SkjelinOttosen
{
    public partial class ViewForm : Form
    {
        // Initialize collection 
        RPG_CharactersCollection characters = new RPG_CharactersCollection();
        // Sets character to null
        RPG_Character selectedCharacter = null;
        public ViewForm()
        {
            InitializeComponent();

            // Adds values to combobox 
            cbCharacterType.Items.Add("Warrior");
            cbCharacterType.Items.Add("Thief");
            cbCharacterType.Items.Add("Wizard");

            // Adds values to combobox 
            cbEnergy.Items.Add(EnergyEnum.High.ToString());
            cbEnergy.Items.Add(EnergyEnum.Medium.ToString());
            cbEnergy.Items.Add(EnergyEnum.Low.ToString());
            cbEnergy.Items.Add(EnergyEnum.Null.ToString());
        }

        // Changes values to match the selected character
        private void cbViewCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCharacter = (RPG_Character) cbViewCharacters.SelectedItem;
            ipName.Text = selectedCharacter.Name;
            ipHP.Text = selectedCharacter.HP.ToString();
            cbEnergy.Text = selectedCharacter.Energy.ToString();
            ipAttackPower.Text = selectedCharacter.AttackPower.ToString();
            ipArmor.Text = selectedCharacter.Armor.ToString();                    
        }

        // This method performs a GetAllCharacter method for the selected character type
        private void cbCharacterType_SelectedIndexChanged(object sender, EventArgs e)
        {
           // Selected character type
            string characterSelected = cbCharacterType.SelectedItem.ToString();

            // Uses helper method to get the character type enum value
            CharacterTypeEnum characterType = InputCheckHelper.CheckCharacterTypeInput(characterSelected);
           
            // Gets all characters
            characters = SQLConnectionHelper.GetAllCharacters(characterType);

            // Adds character to the combo box
            foreach (RPG_Character character in characters.RPG_Characters)
            {
               cbViewCharacters.Items.Add(character);            
            }
        }

        /* Resets the status message when editing the input fields */
        private void ipName_TextChanged(object sender, EventArgs e)
        {
            lbStatus.Text = "";
        }

        private void ipHP_TextChanged(object sender, EventArgs e)
        {
            lbStatus.Text = "";
        }
        private void cbEnergy_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbStatus.Text = "";
        }

        private void ipAttackPower_TextChanged(object sender, EventArgs e)
        {
            lbStatus.Text = "";
        }

        private void ipArmor_TextChanged(object sender, EventArgs e)
        {
            lbStatus.Text = "";
        }

        // This method performs a Delete method for the selected character type
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            SQLConnectionHelper.DeleteCharacter(selectedCharacter);
            lbStatus.Text = "Deleted";
        }

        // This method performs a Update method for the selected character type
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // Split namespace class object to object name
            string characterTypeString = selectedCharacter.GetType().ToString().Split('.')[2];
            CharacterTypeEnum characterType= 0;
            
            // Stores input values
            int id = selectedCharacter.Id;
            string newName = ipName.Text;
            int newHP = Int16.Parse(ipHP.Text);
            EnergyEnum newEnergy = 0; 
            int newAttackPower = Int16.Parse(ipAttackPower.Text);
            int newArmor = Int16.Parse(ipArmor.Text);

            // Uses helper method to get the charater type enum value
            characterType = InputCheckHelper.CheckCharacterTypeInput(characterTypeString.ToString());
            // Uses helper method to get the energy enum value
            newEnergy = InputCheckHelper.CheckEnergyInput(cbEnergy.Text);

            if (SQLConnectionHelper.UpdateCharacter(characterType, id, newName, newHP,newEnergy, newAttackPower, newArmor))
            {
                lbStatus.Text = "Updated";
            }  
        }

        // Return to the menu
        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Close();
            Program.menuForm.Show();
        }
    }
}
