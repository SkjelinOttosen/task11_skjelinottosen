﻿namespace Task11_SkjelinOttosen
{
    partial class ViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbViewCharacters = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCharacterType = new System.Windows.Forms.ComboBox();
            this.Type = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lbName = new System.Windows.Forms.Label();
            this.ipName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ipHP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ipAttackPower = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ipArmor = new System.Windows.Forms.TextBox();
            this.cbEnergy = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(85, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 25);
            this.label3.TabIndex = 11;
            this.label3.Text = "Select character";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Black", 13F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Coral;
            this.label2.Location = new System.Drawing.Point(83, 70);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(245, 37);
            this.label2.TabIndex = 10;
            this.label2.Text = "RPG Characters";
            // 
            // cbViewCharacters
            // 
            this.cbViewCharacters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbViewCharacters.FormattingEnabled = true;
            this.cbViewCharacters.Location = new System.Drawing.Point(240, 227);
            this.cbViewCharacters.Name = "cbViewCharacters";
            this.cbViewCharacters.Size = new System.Drawing.Size(637, 28);
            this.cbViewCharacters.TabIndex = 12;
            this.cbViewCharacters.SelectedIndexChanged += new System.EventHandler(this.cbViewCharacters_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(89, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 22);
            this.label1.TabIndex = 13;
            this.label1.Text = "Select character:";
            // 
            // cbCharacterType
            // 
            this.cbCharacterType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbCharacterType.ItemHeight = 20;
            this.cbCharacterType.Location = new System.Drawing.Point(240, 176);
            this.cbCharacterType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbCharacterType.Name = "cbCharacterType";
            this.cbCharacterType.Size = new System.Drawing.Size(637, 28);
            this.cbCharacterType.TabIndex = 15;
            this.cbCharacterType.Text = " - Select a type -";
            this.cbCharacterType.SelectedIndexChanged += new System.EventHandler(this.cbCharacterType_SelectedIndexChanged);
            // 
            // Type
            // 
            this.Type.AutoSize = true;
            this.Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type.Location = new System.Drawing.Point(89, 177);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(56, 22);
            this.Type.TabIndex = 16;
            this.Type.Text = "Type:";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbName.Location = new System.Drawing.Point(1038, 176);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(62, 22);
            this.lbName.TabIndex = 18;
            this.lbName.Text = "Name:";
            // 
            // ipName
            // 
            this.ipName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ipName.Location = new System.Drawing.Point(1190, 180);
            this.ipName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ipName.Name = "ipName";
            this.ipName.Size = new System.Drawing.Size(274, 26);
            this.ipName.TabIndex = 17;
            this.ipName.TextChanged += new System.EventHandler(this.ipName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(1031, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 25);
            this.label4.TabIndex = 19;
            this.label4.Text = "Edit character";
            // 
            // btn_Delete
            // 
            this.btn_Delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Delete.Location = new System.Drawing.Point(1042, 444);
            this.btn_Delete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(208, 45);
            this.btn_Delete.TabIndex = 20;
            this.btn_Delete.TabStop = false;
            this.btn_Delete.Text = "Delete";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(1256, 444);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(208, 45);
            this.btnUpdate.TabIndex = 20;
            this.btnUpdate.TabStop = false;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(1038, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 22);
            this.label5.TabIndex = 23;
            this.label5.Text = "HP:";
            // 
            // ipHP
            // 
            this.ipHP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ipHP.Location = new System.Drawing.Point(1190, 229);
            this.ipHP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ipHP.Name = "ipHP";
            this.ipHP.Size = new System.Drawing.Size(274, 26);
            this.ipHP.TabIndex = 22;
            this.ipHP.TextChanged += new System.EventHandler(this.ipHP_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(1038, 278);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 22);
            this.label6.TabIndex = 25;
            this.label6.Text = "Energy:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(1038, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 22);
            this.label7.TabIndex = 27;
            this.label7.Text = "Attack Power:";
            // 
            // ipAttackPower
            // 
            this.ipAttackPower.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ipAttackPower.Location = new System.Drawing.Point(1190, 325);
            this.ipAttackPower.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ipAttackPower.Name = "ipAttackPower";
            this.ipAttackPower.Size = new System.Drawing.Size(274, 26);
            this.ipAttackPower.TabIndex = 26;
            this.ipAttackPower.TextChanged += new System.EventHandler(this.ipAttackPower_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(1038, 372);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 22);
            this.label8.TabIndex = 29;
            this.label8.Text = "Armor:";
            // 
            // ipArmor
            // 
            this.ipArmor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ipArmor.Location = new System.Drawing.Point(1190, 375);
            this.ipArmor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ipArmor.Name = "ipArmor";
            this.ipArmor.Size = new System.Drawing.Size(274, 26);
            this.ipArmor.TabIndex = 28;
            this.ipArmor.TextChanged += new System.EventHandler(this.ipArmor_TextChanged);
            // 
            // cbEnergy
            // 
            this.cbEnergy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbEnergy.ItemHeight = 20;
            this.cbEnergy.Location = new System.Drawing.Point(1190, 275);
            this.cbEnergy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbEnergy.Name = "cbEnergy";
            this.cbEnergy.Size = new System.Drawing.Size(274, 28);
            this.cbEnergy.TabIndex = 30;
            this.cbEnergy.SelectedIndexChanged += new System.EventHandler(this.cbEnergy_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Black", 13F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Coral;
            this.label9.Location = new System.Drawing.Point(1028, 74);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(228, 37);
            this.label9.TabIndex = 31;
            this.label9.Text = "RPG Character";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 23);
            this.label10.TabIndex = 0;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbStatus.Location = new System.Drawing.Point(1047, 513);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(0, 25);
            this.lbStatus.TabIndex = 32;
            // 
            // btn_back
            // 
            this.btn_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.Location = new System.Drawing.Point(90, 527);
            this.btn_back.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(208, 45);
            this.btn_back.TabIndex = 33;
            this.btn_back.TabStop = false;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // ViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1898, 1024);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbEnergy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ipArmor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ipAttackPower);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ipHP);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.ipName);
            this.Controls.Add(this.cbCharacterType);
            this.Controls.Add(this.Type);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbViewCharacters);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "ViewForm";
            this.Text = "ViewForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ComboBox cbViewCharacters;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox cbCharacterType;
        internal System.Windows.Forms.Label Type;
        internal System.ComponentModel.BackgroundWorker backgroundWorker1;
        internal System.Windows.Forms.Label lbName;
        internal System.Windows.Forms.TextBox ipName;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Button btn_Delete;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox ipHP;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox ipAttackPower;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox ipArmor;
        internal System.Windows.Forms.ComboBox cbEnergy;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Button btn_back;
    }
}