﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task11_SkjelinOttosen
{
    public static class Program
    {
        public static MenuForm menuForm;
        public static CreateForm createForm;
        public static ViewForm viewForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            menuForm = new MenuForm();
            createForm = new CreateForm();
            viewForm = new ViewForm();
            Application.Run(menuForm);
        }
    }
}
