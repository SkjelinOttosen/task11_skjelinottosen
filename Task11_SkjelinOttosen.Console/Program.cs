﻿using System;
using Task11_SkjelinOttosen.Model;
using Task11_SkjelinOttosen.SQLHelper;

namespace Task11_SkjelinOttosen.Model
{
    // Battle simulation demo
    class Program
    {
        static void Main(string[] args)
        {
            // Warrior Frank
            Sword sword = new Sword("Regulare sword", 10);
            Warrior frank = new Warrior("Frank", sword);

            // Thief Sarah 
            Knife knife = new Knife("Regulare knife", 3);
            Thief sarah = new Thief("Sarah", knife);


            /*  Battle simulation demo:
             *  Demonstrate the functionality of the characters in a battle scenario
             *  Propeties such as Name, HP, Energy, AttackPower, Armor 
             *  Methods such as Attack, Block, Hit And DrinkEnergyPotion
            */

            Console.WriteLine("**********************");
            Console.WriteLine("Battle simulation demo");
            Console.WriteLine("**********************\n");
           
            // Round 1
            Console.WriteLine("Round 1");
            sarah.Hit(frank.Attack(frank.Weapon));
            Console.WriteLine($"Status {sarah.Name}: HP: {sarah.HP } Armor: {sarah.Armor}\n");
            frank.Hit(sarah.Attack(sarah.Weapon));
            Console.WriteLine($"Status {frank.Name}: HP: {frank.HP } Armor: {frank.Armor}\n");

            // Round 2
            Console.WriteLine("Round 2");
            sarah.Hit(frank.Attack(frank.Weapon));
            Console.WriteLine($"Status {sarah.Name}: HP: {sarah.HP } Armor: {sarah.Armor}\n");
            frank.Hit(sarah.Attack(sarah.Weapon));
            Console.WriteLine($"Status {frank.Name}: HP: {frank.HP } Armor: {frank.Armor}\n");

            // Restores Energy
            frank.DrinkEnergyPotion();
            // Round 3
            Console.WriteLine("Round 3");
            sarah.Hit(frank.Attack(frank.Weapon));
            Console.WriteLine($"Status {sarah.Name}: HP: {sarah.HP } Armor: {sarah.Armor}\n");
            frank.Hit(sarah.Attack(sarah.Weapon));
            Console.WriteLine($"Status {frank.Name}: HP: {frank.HP } Armor: {frank.Armor}\n");
        }
    }
}
