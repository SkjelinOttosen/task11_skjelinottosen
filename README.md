Description:
RPG Character is an application that allows the user to create warriors, thiefs and wizards in a graphical user interface. The characters are saved in a database, or in a CSV file if the SQL connection fails. New characters gets default values ​associated with each characters type. Afterwards the user can read, change and delete character from the database.

Console demo:
The user can also run a battle simulation demo in the console that shows the functionality of the characters with attacks, defence, weapons and energy potion.

Architecture
The application has a layered structure where the model classes, the user interface, SQLConnectionHjeper class and the console demo are separate into different projects. 