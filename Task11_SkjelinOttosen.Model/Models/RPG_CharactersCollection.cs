﻿using System;
using System.Collections.Generic;


// Enum values for character types
public enum CharacterTypeEnum
{
    Warrior,
    Thief,
    Wizard
}
namespace Task11_SkjelinOttosen.Model
{
    // Collection class for storing RPG Characters.
    public class RPG_CharactersCollection 
    {
        public  List<RPG_Character> RPG_Characters { get; set; }

        public RPG_CharactersCollection()
        {
            RPG_Characters = new List<RPG_Character>();
        }

        // Adds a character to the collections
        public void Add(RPG_Character character)
        {
            RPG_Characters.Add(character);
            Console.WriteLine($"{character} is added in the collection");

        }
        // Remoeves a character form the collection
        public void Remove(RPG_Character character)
        {
            RPG_Characters.Remove(character);
            Console.WriteLine($"{character} is removed from the collection");
        }
    }
}
