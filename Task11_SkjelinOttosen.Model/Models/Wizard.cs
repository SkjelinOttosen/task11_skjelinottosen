﻿

namespace Task11_SkjelinOttosen.Model
{
    public class Wizard : RPG_Character
    {
        public MagicWand MagicWand { get; set; }

        public Wizard() : base()
        {
            base.AttackPower = 13;
            base.Armor = 3;
        }
        public Wizard(string name) : base(name)
        {
            base.AttackPower = 13;
            base.Armor = 3;
        }
        public Wizard(string name,  Weapon weapon) : base(name)
        {
            base.AttackPower = 13;
            base.Armor = 3;
            base.Weapon = weapon;
        }
    }
}
