﻿

namespace Task11_SkjelinOttosen.Model
{
    public class Warrior : RPG_Character
    {
        public Warrior(): base()
        {
            base.AttackPower = 10;
            base.Armor = 30;

        }
        public Warrior(string name) : base(name)
        {
            base.AttackPower = 10;
            base.Armor = 30;
        }
        public Warrior(string name, Weapon weapon) : base(name)
        {
            base.AttackPower = 10;
            base.Armor = 20;
            base.Weapon = weapon;
        }  
    }
}
