﻿
using System;


namespace Task11_SkjelinOttosen.Model
{
    // Enum for enerygy types
    public enum EnergyEnum
    {
        Null = 0,
        Low = 3,
        Medium = 2,
        High = 1
    }
    public abstract class RPG_Character
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int HP { get; set; }

        public Enum Energy { get; set; }

        public int AttackPower { get; set; }

        public int Armor { get; set; }

        public Weapon Weapon { get; set; }

        public RPG_Character()
        {
            HP = 100;
            Energy = EnergyEnum.High;
        }
        public RPG_Character(string name)
        {
            Name = name;
            HP = 100;
            Energy = EnergyEnum.High;
        }
        public RPG_Character(string name, Weapon weapon)
        {
            Name = name;
            HP = 100;
            Energy = EnergyEnum.High;       
        }

        // Attack method for the character
        // The return value (damage) of this method will vary depending on the type of character using it
        // The value of AttackPower and Weapon damage will vary form each type of character
        // Each attack will cost a level of energy. The attacks become weaker when the energy decrease

        public virtual int Attack(Weapon weapon)
        {
            if (Convert.ToInt16(Energy) != Convert.ToInt16(EnergyEnum.Null))
            {
                int weaponDamage = weapon.AttackDamage();
                int damage = AttackPower + weaponDamage / Convert.ToInt16(Energy);

                // Decrease energy level
                DecreaseEnergy(Energy);

                Console.WriteLine($"{Name} attacks with {damage} damage");
                return damage;
            }
            else
            {
                Console.WriteLine("Energy too low for attack");
                return 0;
            }       
        }

        // Method for encrease energy to make attackpower more powerfull in the next attack
        public virtual void EncreaseEnergy(Enum energy)
        {
            if (Convert.ToInt16(Energy) == Convert.ToInt16(EnergyEnum.Null))
            {
                Energy = EnergyEnum.Low;

            }
            else if (Convert.ToInt16(Energy) == Convert.ToInt16(EnergyEnum.Low))
            {
                Energy = EnergyEnum.Medium;
            }
            else
            {
                Energy = EnergyEnum.High;
            }
        }

        // Method for decrease energy to lower attackpower in the next attack
        public virtual void DecreaseEnergy(Enum energy)
        {
            if (Convert.ToInt16(Energy) == Convert.ToInt16(EnergyEnum.High))
            {
                Energy = EnergyEnum.Medium;

            }
            else if (Convert.ToInt16(Energy) == Convert.ToInt16(EnergyEnum.Medium))
            {
                Energy = EnergyEnum.Low;
            }
            else
            {
                Energy = EnergyEnum.Null;
            }
        }

        // Block method for the attack
        public virtual bool Block() 
        {
            // Makes random value for simulate the the chances of a successful block
            Random random = new Random();
            int randomInt = random.Next(0, 2);

            if (randomInt == 1)
            {
                Console.WriteLine("Blocking attack failed ");
                return false;
            }
            else
            {
                Console.WriteLine("Blocking attack successfully ");
                return true;
            }    
        }

        public virtual void Hit(int damage)
        {
            if(damage > 0)
            {
                // Checks if character is not dead
                if (HP > 0)
                {
                    // Block method for the attack
                    if (!Block())
                    {
                        // Checks if damage is greater than armor
                        if (damage < Armor)
                        {
                            Armor -= damage;
                        }
                        else
                        {
                            // Calclulates damage left for HP
                            int hpDamage = damage - Armor;

                            // Calculates damage for Armor
                            int armorDamage = damage - hpDamage;

                            // Decrease Armor
                            Armor -= armorDamage;

                            // Decrease HP
                            HP -= hpDamage;

                            // Checks if character is K.O
                            if (HP <= 0)
                            {
                                // Sets HP to 0 and declare character dead
                                HP = 0;
                                Console.WriteLine("K.O!");
                            }
                            else
                            {
                                Console.WriteLine($"{Name } was getting a hit with {damage} damage!");
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("It's not nice to attack the dead");
                }
            }
            else
            {
                Console.WriteLine("The attack had no effect");
            }       
        }


        // Method for restoring energy
        public void DrinkEnergyPotion()
        {
            Energy = EnergyEnum.High;
            Console.WriteLine($"{Name} drinks energy potion and gets full energy\n");
        }

        // To string method is used to display object in the user interface
        public override string ToString()
        {
            return $"Id: {Id} Name: {Name} HP: {HP} Energy: {Energy} AttackPower: {AttackPower} Armor: {Armor}";
        }
    }
}
