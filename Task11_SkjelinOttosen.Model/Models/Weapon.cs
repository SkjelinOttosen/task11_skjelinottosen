﻿

namespace Task11_SkjelinOttosen.Model
{
    public abstract class Weapon
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Damage { get; set; }

        public int Capacity { get; set; }

        public Weapon(string name, int damage)
        {
            Name = name;
            Damage = damage;
        }

        public Weapon (string name, int damage, int capacity)
        {
            Name = name;
            Damage = damage;
            Capacity = capacity;
        }

        // The attack damage of the weapon. Attack power is one of the variables that make up the damage in an attack
        public virtual int AttackDamage()
        {
            return Damage;
        }

        public override string ToString()
        {
            return $" Id: {Id} Name: {Name} Damage:{Damage} Capacity{Capacity}";
        }
    }
}
