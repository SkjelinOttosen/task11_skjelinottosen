﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Task11_SkjelinOttosen.Model
{
    public class Thief : RPG_Character
    {
        public Knife Knife { get; set; }

        public Thief() : base()
        {
            base.AttackPower = 6;
            base.Armor = 10;
        }
        public Thief(string name) : base(name)
        {
            base.AttackPower = 6;
            base.Armor = 10;
        }

        public Thief(string name, Weapon weapon) : base(name)
        {
            base.AttackPower = 6;
            base.Armor = 10;
            base.Weapon = weapon;
        }
    }
}
