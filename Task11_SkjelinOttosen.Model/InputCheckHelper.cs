﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11_SkjelinOttosen.Model
{
    // Helper class that takes string input and returns the matching enums and objects
    public static class InputCheckHelper
    {
        // Checks input and returns the matching enum
        public static EnergyEnum CheckEnergyInput(string energyInput)
        {
            // Checks energy enum value
            switch (energyInput)
            {
                case "High":
                    return EnergyEnum.High;
                case "Medium":
                    return EnergyEnum.Medium;
                case "Low":
                   return  EnergyEnum.Low;
            }
             return EnergyEnum.Null;
        }

        // Checks character type input and returns the matching enum
        public static CharacterTypeEnum CheckCharacterTypeInput(string characterTypeInput)
        {
            // Checks type of character and creates  matching objects
            switch (characterTypeInput)
            {
                case "Warrior":
                    return CharacterTypeEnum.Warrior;
                case "Thief":
                    return CharacterTypeEnum.Thief;
                case "Wizard":
                    return CharacterTypeEnum.Wizard;
            }
            return 0;
        }

        // Create an empty object from input
        public static RPG_Character CreateEmptyRPG_CharacterFromInput(string characterTypeInput)
        {
            // Checks type of character and creates  matching objects
            switch (characterTypeInput)
            {
                case "Warrior":
                    return new Warrior();
                case "Thief":
                    return new Thief();                   
                case "Wizard":
                    return new Wizard();            
            }

            return null;
        }
    }
}
